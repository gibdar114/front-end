import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//main page
import {MainComponent} from './main/main.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: 'home', component:MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
